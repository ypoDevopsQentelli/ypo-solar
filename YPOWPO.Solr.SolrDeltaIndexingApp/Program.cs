﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace YPOWPO.Solr.SolrDeltaIndexing
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                AddToTextFile("Start");

                // Begin logging.
                AddToTextFile("HttpWebRequest");

                // Get the list of Solr URLs to hit.
                string solrRequestUrls = ConfigurationManager.AppSettings["SolrRequestUrl"];

                // Decode the URLs as a bunch.
                solrRequestUrls = HttpUtility.UrlDecode(solrRequestUrls);

                // Remove ALL odd characters. Tabs, newlines, returns, SPACEs... Solr hates them all.
                solrRequestUrls = Regex.Replace(solrRequestUrls, @"\t|\n|\r|\s", "");

                // Split all the URLs apart and put them neatly in a list.
                var urlList = new List<string>(solrRequestUrls.Split(';'));

                // Make a web request to each URL and get the response.
                foreach (string decodedUrl in urlList)
                {
                    int retries = 0;
                    bool requestSuccess = false;

                    // While we haven't run out of retries and we still haven't succeeded in getting a good resonse, try the urls.
                    while (retries < int.Parse(ConfigurationManager.AppSettings["MaxRetries"]) && !requestSuccess)
                    {
                        // Try/catch each webrequest individually. This way the failure of one won't cause the whole thing to stop.
                        try
                        {
                            WebRequest webrequest = WebRequest.Create(decodedUrl.Trim());

                            // Pull in the timeout setting from AppSettings.config (multiplied by 1000, Timeout is in milliseconds.
                            webrequest.Timeout = 1000 * int.Parse(ConfigurationManager.AppSettings["Timeout"]);
                            using (StreamReader streamReader = new StreamReader(webrequest.GetResponse().GetResponseStream()))
                            {
                                streamReader.ReadToEnd();
                                streamReader.Close();
                            }

                            // Log success.
                            requestSuccess = true;
                            AddToTextFile("Success for - " + decodedUrl);
                        }
                        catch (Exception ex)
                        {
                            retries++;
                            // Log failure.
                            AddToTextFile("Exception for - " + decodedUrl);
                            AddToTextFile(ex.ToString());

                            // If we have more retyring to do, log the fact that we're trying again.
                            if (retries < int.Parse(ConfigurationManager.AppSettings["MaxRetries"]))
                            {
                                AddToTextFile("Retrying...");
                            }

                            // If we're all out of retries, log ultimate failure.
                            else
                            {
                                AddToTextFile("Failure for - " + decodedUrl);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                // Log failure.
                AddToTextFile(ex.ToString());
            }
            finally
            {
                // Log finish.
                AddToTextFile("Finish");
                AddToTextFile("");
            }
        }

        internal static void AddToTextFile(string message)
        {
            string logFilePath = ConfigurationManager.AppSettings["LogFilePath"];

            // Stamp the logfile with today's date.
            string str = ConfigurationManager.AppSettings["LogFileName"].Replace("{date}", DateTime.Now.ToString("yyyyMMdd"));

            // If the directory for our logs doesn't exist, create it.
            if (!Directory.Exists(logFilePath))
            {
                Directory.CreateDirectory(logFilePath);
            }

            // Write the log.
            using (StreamWriter streamWriter = File.AppendText(logFilePath + "\\" + str))
            {
                streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "- " + message);
                streamWriter.Close();
            }
        }
    }
}
