# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#-----------------------------------------------------------------------

#common english names
# real name(s) => all names that should match  (real name, nicknames, etc).
# notes
# 1. Names on the left of the => should only appear on the left on a single row.
# 2. Names on the right of the => need not be unique.
# 3. Real Names to be preserved should exist on both sides of the line.

Abigail => Abigail, Abbie, Abby, Gail, Nabby
Ada => Ada, Adie
Adelaide => Adelaide, Addie, Adela, Dell, Della
Adele, Adelle => Adele, Adelle, Adela, Addie, Dell, Della
Adeline => Adeline, Adelina, Adaline, Addie, Aline, Dell, Della
Adrienne => Adrienne, Adriana, Adie
Agatha => Agatha, Aggie
Agnes => Agnes, Aggie, Ness, Nessie
Aileen => Aileen, Eileen, Allie, Lena
Eileen => Aileen, Eileen, Lena
Alberta => Allie, Bertie
Alexandra, Alexandria => Alexandra, Alexandria, Alex, Alix, Alexa, Alla, Allie, Ali, Lexy, Sandra, Sandy
Alexis => Alexis, Alex
Alfreda => Alfreda, Alfie, Alfy, Frieda, Freda, Freddie, Freddy
Alice => Alice, Alicia, Alyce, Alisa, Alissa, Alyssa, Allie, Ally, Ali, Elsie, Lisa
Alison, Allison, Allyson, Allyson => Alison, Allison, Alyson, Allyson, Allie, Ally, Ali
Althea => Althea, Thea
Amabel => Amabel, Mabel, Mab, Mabs, Mabbie
Amanda => Amanda, Mandy, Mandi
Amelia => Amelia, Amy, Millie, Milly
Amy, Aimee, Amie
Anastasia => Anastasia, Ana, Stacy
Andrea => Andrea, Andy
Angela, Angelica, Angelina, Angeline => Angela, Angelica, Angelina, Angeline, Angela, Angel, Angie
Anita => Anita, Ana, Nita
Anna, Ann, Anne, Annie => Anna, Ann, Anne, Annie, Nance, Nancy, Nancie, Nan, Nana, Nanny 
Annabel, Annabelle => Annabel, Annabelle, Anabel, Ann, Anna, Bel, Belle, Bell
Annette => Annette, Annetta, Annie, Netta, Nettie, Netty, Nanette, Nannette
Antoinette => Antoinette, Nettie, Netty, Net, Netta, Toni, Tony, Toy, Toi
Antonia => Antonia, Toni, Tony, Tonya, Tonia
Arabella => Arabella, Arabel, Arabelle, Bel, Bell, Belle, Bella
Arlene => Arlene, Arline, Arleen, Arlyne, Lena, Arly, Lynn
Ashley => Ashley, Ash
Audrey => Audrey, Dee
Augusta, Augustina => Augusta, Augustina, Aggy, Augie, Gussie, Gusty, Ina, Tina
Aurora => Aurora, Rori
Barbara => Barbara, Bab, Babs, Babbie, Barbie, Babette
Beatrice => Beatrice, Beatrix, Bea, Bee, Beattie, Trixie, Trissie
Belinda => Belinda, Bel, Bell, Belle, Linda, Lindy, Lin, Lynn
Bernice => Bernice, Bernie
Bertha => Bertha, Berta, Bertie
Beverly, Beverley => Beverly, Beverley, Bev
Blanche => Blanche, Blanch
Bonnie, Bonny
Brenda => Brenda, Brendie, Brandy
Bridget, Bridgette, Brigid, Brigit, Bridgit  => Bridget, Bridgette, Brigid, Brigit, Bridgit, Biddie, Biddy, Bridie, Bridey, Brie, Bree, Brita
Brittany, Brittney, Britney => Brittany, Brittney, Britney, Brit, Britt, Brita, Brie
Cameron, Camryn => Cameron, Camryn, Cammy, Cami
Camille => Cammille, Cammie, Cammy, Cami
Camilla => Camilla, Cammie, Cammy, Cami
Candace, Candice => Candace, Candice, Candy
Caren, Carin, Caryn, Karen, Karin, Karyn
Carla, Karla => Carla, Karla, Carlie, Carly
Carlotta => Carlotta, Carlota, Lotta, Lottie, Lotty
Carol, Carole, Carrol, Carroll, Karol
Caroline, Carolyn, Carolina => Caroline, Carolyn, Carolina, Carlyne, Carline, Karoline, Carrie, Carry, Carlie, Carly, Callie
Cassandra => Cassandra, Cass, Cassie, Cassey, Casey, Sandra, Sandy
Catherine, Cathryn, Catheryn, Catharine, Katherine, Katharine, Kathryn, Kathrynne => Catherine, Cathryn, Catheryn, Catharine, Cattie, Catty, Cathie, Cathy, Cassie, Kit, Kitty, Kittie, Catie, Katie, Katherine, Katharine, Kathryn, Kathrine, Kathrynne, Kate, Kathie, Kathy, Katie, Katy, Kay, Katty, Kattie, Kit, Kitty, Kittie
Cathleen, Kathleen => Cathleen, Kathleen, Kitty, Kittie, Catie, Katie, Kath, Kathy, Cathy, Cattie, Kattie, Kitty
Cecilia, Cecillia, Cecelia, Cecile, Cecily, Cicely, Celia
Charity => Charity, Chattie, Chatty, Cherry
Charlotte => Charlotte, Lotta, Lottie, Lotty, Lola, Lolita, Chattie, Charlie
Cheryl => Cheryl, Cherie
Christine, Christina, Christiana => Christine, Christina, Christiana, Chris, Christy, Christie, Christa, Chrissie, Kit, Tina
Clara, Clare, Clair, Claire
Clarice, Clarissa
Claudia => Claudia, Claudie
Clementine, Clementina => Clementine, Clementina, Clem, Clemmie, Tina
Colleen, Coleen
Constance => Constance, Connie, Connee
Cora => Cora, Corrie
Cordelia => Cordelia, Cordy, Delia
Corinne, Corinna => Corinne, Corinna, Cora, Corrie
Cornelia => Cornelia, Connie, Corny, Nell, Nellie
Courtney => Courtney, Court, Courtie
Crystal, Krystal => Crystal, Krystal, Chrystal,  Christal, Crys, Chris
Cynthia => Cynthia, Cindy
Daisy => Daisy, Daysie
Danielle, Daniela => Danielle, Daniela, Dani, Danny
Daphne => Daphne, Daphie
Darlene, Darleen, Darlyne => Darlene, Darleen, Darlyne, Lena, Darla
Deborah, Debbie, Debby, Debra
Delia => Delia, Dell, Della
Denise, Denice => Denise, Denice, Denny
Diane, Dianne => Diane, Dianne, Di
Dinah => Dinah, Dina, Di
Dolores => Dolores, Delores, Lola, Lolita
Dominique => Dominique, Minnie, Nicki, Nikki
Dominica => Dominica, Minnie, Nicki, Nikki
Dora => Dora, Dorrie, Dori, Dory
Dorrie, Dori, Dory
Doreen, Dorene => Doreen, Dorene, Dorrie, Dori, Dory
Doris => Doris, Dorris, Dorrie, Dory
Dorothy => Dorothy, Dora, Dorrie, Doll, Dolly, Dodie, Dot, Dottie, Dotty, Dee
Dorothea => Dorothea, Dora, Dorrie, Doll, Dolly, Dodie, Dot, Dottie, Dotty, Dee
Edith => Edith, Edyth, Edythe, Edie, Edye, Dee
Edna => Edna, Eddie
Elaine, Helaine => Elaine, Helaine, Ellie, Elly, Lainie
Eleanor, Elinor => Eleanor, Elinor, Ella, Ellie, Elly, Nell, Nellie, Nelly, Nora, Lally, Lallie
Elisa, Elissa, Elyssa, Alissa, Alyssa
Elizabeth, Elisabeth => Elizabeth, Elisabeth, Betty, Bettie, Bet, Bett, Bette, Betta, Betsy, Betsey, Betsi, Beth, Bess, Bessie, Bessy, Bettina, Elsie, Elisa, Elsa, Eliza, Ellie, Elly, Ilse, Liz, Lizzy, Lizzie, Liza, Lisa, Lise, Lisette, Lizette, Lisbet, Lizbeth, Libby, Bat, Batty
Eloise, Heloise => Eloise, Heloise, Lois
Elvira => Elvira, Elvie
Emily, Emilia => Emily, Emilia, Emmy, Emmie, Millie, Milly
Emma => Emma,  Emmy, Emmie
Erica, Erika, Ericka => Erica, Erika, Ericka, Ricky, Rickie
Ernestine, Earnestine => Ernestine, Earnestine, Erna, Ernie, Tina
Estelle, Estella => Estelle, Estella, Essie, Stella
Esther, Ester, Hester => Esther, Ester, Hester, Essie, Ettie, Etty, Hettie, Hetty
Etta => Etta, Ettie, Etty
Eugenia => Eugenia, Gene, Genie
Eulalia => Eulalia, Eula, Lally, Lallie
Eunice => Eunice, Euny, Eunie
Eustacia => Eustacia, Stacy, Stacey, Stacia
Eve, Eva, Evie, Evy
Eveline, Evelyn, Evelina => Eveline, Evelyn, Evelina, Eve, Evie, Evy, Lynn
Evangeline, Evangelina => Evangeline, Evangelina, Eve, Evie, Evy, Angie, Lynn
Faith => Faith, Fae, Fay, Faye
Felicia => Felicia, Felice, Fee, Fi
Felicity => Felicity, Felice, Fee, Fi
Florence => Florence, Flo, Floy, Floss, Flossie, Flora, Florrie
Frances => Frances, Fan, Fannie, Fanny, Fran, Frannie, Franny, Francie, Francy, France, Frankie, Franky
Francesca => Francesca, Francisca, Fran, Cesca
Francine => Francine, Fan, Fannie, Fanny, Fran, Frannie, Franny, Francie, Francy, France, Frankie, Franky
Frederica, Frederika, Fredericka => Frederica, Frederika, Fredericka, Freda, Freddie, Freddy, Ricky, Rickie
Gabrielle, Gabriela, Gabriella => Gabrielle, Gabriela, Gabriella, Gabby, Gabi, Gaby
Genevieve => Genevieve, Gene, Ginny, Jenny, Viv
Georgina => Georgina, Georgine, Georgie, Gina
Geraldine => Geraldine, Gerry, Gerrie, Gerri, Jerry, Dina
Gertrude => Gertrude, Gertie, Trudie, Trudy
Gillian, Jillian
Grace => Grace, Gracie
Gwendolen, Gwendolyn => Gwendolen, Gwendolyn, Gwen, Gwenda, Wendy
Hannah => Hannah, Hanna, Ann, Annie, Nana, Nanny
Harriet => Harriet, Hattie, Hatty
Heather => Heather, Hettie, Hetty
Helen, Helena => Helen, Helena, Elena, Ellen, Nell, Nellie, Nelly, Ellie, Elly, Lena, Lala, Lally, Lallie
Henrietta => Henrietta, Etta, Ettie, Etty, Hettie, Hetty, Nettie, Netty
Hillary, Hilary => Hillary, Hilary, Hill, Hillie
Imogen, Imogene => Imogen, Imogene, Immy, Immie
Irene => Irene, Renie, Rena
Isabel, Isabelle, Isabella => Isabel, Isobel, Isabelle, Isabella, Bel, Bell, Belle, Bella, Issy
Jacqueline, Jacquelyn => Jacqueline, Jacquelyn, Jackie, Jacky
Jane => Jane, Janie, Janey, Jenny, Jennie, Jen, Janet
Janet, Janette => Janet, Janette, Janetta, Jan, Nettie, Netty, Netta
Janice, Janis =>  Janice, Janis, Jenice, Jan
Jean, Jeanne => Jean, Jeanne, Jeanie, Jeannie
Jeannette, Jeannetta, Jeanette, Junette => Jeannette, Jeannetta, Jeanette, Junette, Jeanie, Jeannie, Nettie, Netty, Netta
Jemima => Jemima, Jem, Jemma, Mima, Mimi
Jennifer => Jennifer, Jen, Jenny, Jennie, Jenne
Jessica => Jessica, Jess, Jessie
Joanna => Joanne, Joann, Johanna, Joan, Jo, Jody
Joceline => Jocelyn, Jo, Lynn
Josephine => Jo, Josie, Josey, Jozy, Jody
Joyce => Joyce, Joy
Judith => Judith, Judy, Judie, Jude, Jody, Jodie
Julia => Julia, Julie, Jule
Julianne, Juliana => Julianne, Juliana, Julie, Jule
Juliet, Juliette => Juliet, Juliette, Julie, Jule
Kelly, Kelley
Kimberly, Kimberley
Kristina, Kristin, Kristine, Kristen => Kristina, Kristin, Kristine, Kristen, Kris, Kristi, Kristy, Kristie, Krista
Laura, Lora => Laura, Lora, Laurie, Lori 
Lauren => Lauren, Laurie, Lori
Laurel => Laurel, Laurie, Lori
Laverne, Laverna
Lavinia => Lavinia, Vina, Vinnie
Leah, Lea, Lee, Leigh
Leonora, Leonore, Lenora, Lenore => Leonora, Leonore, Lenora, Lenore, Nora
Leslie, Lesley
Leticia, Letitia => Leticia, Letitia, Lettie, Tisha
Lillian, Lilian => Lillian, Lilian, Lily, Lilly, Lili, Lilli, Lil, Lillie
Lily, Lilly, Lili, Lilli, Lillie
Linda, Lynda => Linda, Lynda, Lindy, Lin, Lynn, Lynne
Lorraine, Loraine => Lorraine, Loraine, Lora, Lori, Lorie
Loretta => Loretta, Etta, Lorrie, Lori, Retta
Lottie, Lotty, Lotta
Louise, Louisa => Louise, Louisa, Lou, Lu, Lulu, Lula, Lois
Lucille, Lucile => Lucille, Lucile, Lucy, Lucky
Lucinda => Lucinda, Lu, Lucy, Lucky, Cindy
Lucy, Lucie => Lucy, Lucie, Lucia, Lulu, Luce, Lucky
Lydia => Lydia, Liddy, Lyddie
Lynn, Lynne
Mabel, Mabelle, Mable => Mabel, Mabelle, Mable, Mab, Mabs, Mabbie
Madeline, Madeleine, Madelyn => Madeline, Madeleine, Madelyn, Maddie, Maddy, Mady
Magdalena => Magdaline, Magdalen, Magdalena, Magda, Magsie, Lena, Maggie
Marcia => Marcia, Marcie, Marcy, Marci
Margaret, Marguerite, Margret => Margaret, Marguerite, Margret, Maggie, Marge, Margie, Marjorie, Margery, Madge, Margot, Margo, Magsie, Maisie, Daisy, Mamie, Peggy, Meg, Mae, May
Margarita => Margarita, Maggie, Marge, Margie, Marjorie, Margery, Madge, Margot, Margo, Magsie, Maisie, Daisy, Mamie, Peggy, Rita, Meg, Mae, May
Marianne, Maryann, Maryanne 
Marilyn, Marilynn, Marylin => Marilyn, Marilynn, Marylin, Lynn
Maribel, Maribelle => Maribel, Maribelle, Belle
Marietta => Marietta, Mariette, Mary, Etta, Ettie, Etty
Marjorie, Marjory, Margery => Marjorie, Marjory, Margery, Marge, Margie
Martha, Marta => Martha, Marta, Marty, Mat, Mattie, Matty, Pat, Pattie, Patty
Mae, May
Moll, Molly, Mollie, Polly
Matilda, Mathilda => Matilda, Mathilda, Mat, Matty, Mattie, Maud, Maude, Patty, Pattie, Tilda, Tillie, Tilly
Maud, Maude => Maude, Maudie, Maudy
Maureen => Maureen, Maury
Maxine => Maxine, Max, Maxie
Melanie => Melanie, Mel, Mellie
Melinda => Melinda, Mel, Mellie, Linda, Mindy
Melissa => Melissa, Mel, Mellie, Missie, Missy, Lisa, Lissa
Mercedes => Mercedes, Mercy, Sadie
Meredith => Meredith, Merry
Michelle, Michele => Michelle, Michele, Mickey, Shelly
Mildred => Mildred, Millie, Milly
Millicent, Milicent => Millicent, Milicent, Millie, Milly
Minnie, Minna, Mina
Miranda => Miranda, Randy
Monica => Monica, Nicki
Myra, Mira
Nadine => Nadine, Nada, Dee
Natalie, Nathalie => Natalie, Nathalie, Nattie, Natty
Natalia => Natalie, Nattie, Natty
Natasha => Natasha, Nattie, Natty
Nell, Nelle, Nellie, Nelly
Nettie, Netty, Netta
Nicole => Nicole, Nicky, Nicki, Nikki, Nikky
Norma => Norma, Normie
Octavia => Octavia, Tave, Tavy, Tavia
Olive, Olivia => Olive, Olivia, Ollie, Olly, Nollie, Livvy, Livia
Pamela => Pamela, Pam, Pammie, Pammy
Patricia => Patricia, Pat, Pattie, Patty, Patsy, Paddy, Tricia, Trisha, Trissie
Paula, Paulina, Pauline => Paula, Paulina, Pauline, Paulie
Penelope => Penelope, Pen, Penny
Phoebe, Phebe
Phyllis => Phyllis, Phyllie, Phil
Priscilla => Priscilla, Prissy
Prudence => Prudence, Prudie, Prudy, Prue, Pru
Rachel => Rachel, Rachie, Rae, Ray
Raquel => Raquel, Kelly, Kellie
Rebecca => Rebecca, Beck, Becky, Reba
Regina => Regina, Reggie, Ray, Gina, Ginny, Rena
Renata => Renata, Renate, Nata, Natie, Rennie, Renny, Renae
Renee, Rene, Renae
Roberta => Roberta, Robbie, Robby, Robin, Robyn, Bobbie, Bobby, Berta, Bertie
Robin, Robyn
Rose, Rosa, Rosie, Rosy => Rose, Rosa, Rosie, Rosy, Ros
Rosabel, Rosabelle, Rosabella => Rosabel, Rosabelle, Rosabella, Rose, Rosie, Rosy, Bell
Rosalie, Rosalee => Rosalie, Rosalee, Rose, Rosie, Rosy
Rosaline, Rosalyn => Rosaline, Rosalyn, Rose, Rosie, Rosy
Rosalind, Rosalinda => Rosalind, Rosalinda, Rose, Rosie, Rosy, Linda
Roseanna, Rosanna, Rosanne => Roseanna, Rosanna, Rosanne, Rose, Rosie, Rosy
Rosemary, Rosemarie => Rosemary, Rosemarie, Rose, Rosie, Rosy
Roxanne, Roxanna, Roxana => Roxanne, Roxanna, Roxana, Roxie, Roxy
Rubina => Rubina, Ruby, Rubie
Ruth => Ruth, Ruthie
Sabrina => Sabrina, Sabina, Brina, Bina, Sabby
Samantha => Samantha, Sam, Sammie, Sammy
Sarah, Sara => Sarah, Sara, Sal, Sally, Sallie
Sharon, Sharron
Shirley, Shirlee, Shirlie
Sibyl, Sybil, Sibylle, Syble
Sonia, Sonya
Sophia, Sophie, Sophy
Stacy, Stacey, Stacie, Staci
Stephanie, Stephania, Stephana, Stefanie, Stefania, Stefana
Susan, Susanna, Susannah, Susanne, Suzanne => Susan, Susanna, Susannah, Susanne, Suzanne, Sue, Susie, Susi, Susy, Suzie, Suzy, Sukie
Sylvia, Silvia, Sylvie
Tabitha => Tabitha, Tabby
Tamara, Tamar => Tamara, Tamar, Tammy, Tammie
Tanya, Tania
Teresa, Theresa, Therese => Teresa, Theresa, Therese, Terry, Terri, Teri, Terrie, Tess, Tessa, Tessie, Tracy, Trissie
Tiffany => Tiffany, Tiff, Tiffy
Tracy, Tracey, Tracie, Traci
Ursula => Ursula, Ursa, Ursie, Sulie
Valentina => Valentina, Val, Vallie
Valerie, Valery, Valeria => Valerie, Valery, Valeria, Val, Vallie
Vanessa => Vanessa, Van, Vannie, Nessa
Veronica => Veronica, Nicky, Nicki, Ronnie, Ronni, Ronny
Victoria => Victoria, Vic, Vick, Vickie, Vicky, Vicki, Viki, Vikki
Violet, Violette, Violetta => Violet, Violette, Violetta, Lettie
Virginia => Virginia, Ginger, Ginny, Jinny, Jenny, Virgie
Vivian, Vivien, Vivienne
Wilhelmina => Wilhelmina, Willa, Wilma, Willie, Billie, Mina, Minnie
Winifred => Winifred, Winnie, Freda, Freddie
Yolanda, Yolande
Yvonne => Yvonne, Vonnie, Vonna, Vana
Yvette => Yvette, Vettie, Vetta
Zoe, Zoey, Zooey
Aaron => Aaron, Aron, Ron, Ronnie, Ronny
Abel => Abel, Abe, Abie
Abner => Abner, Ab, Abbie
Abraham => Abraham, Abe, Abie, Bram
Adam => Adam, Ade
Adrian => Adrian, Ade
Alan, Allan, Allen
Albert => Albert, Al, Bert, Bertie
Alexander, Alexandre => Alexander, Alexandre, Al, Alex, Alec, Aleck, Lex, Sandy, Sander
Alfred => Alfred, Al, Alf, Alfie, Fred, Freddie, Freddy
Algernon => Algernon, Algie, Algy, Alger
Alonso, Alonzo => Alonso, Alonzo, Al, Lon, Lonnie, Lonny
Alvin => Alvin, Alwin, Alwyn, Al, Vin, Vinny, Win
Andrew => Andrew, Andy, Drew
Andre, Andres, Andreas => Andre, Andres, Andreas, Andy
Anthony, Antony, Anton => Anthony, Antony, Anton, Tony
Archibald => Archibald, Arch, Archie, Baldie
Arnold => Arnold, Arnie
Arthur => Arthur, Art, Artie
Augustus, August, Augustine => Augustus, August, Augustine, Augie, Gus, Gussy, Gust, Gustus
Austin, Austen
Baldwin => Baldwin, Baldie, Win
Barrett => Barrett, Barry, Barrie
Bartholomew => Bartholomew, Bart, Barty, Bartlett, Bartley, Bat, Batty
Basil => Basil, Baz, Basie
Benedict => Benedict, Ben, Bennie, Benny
Benjamin => Benjamin, Ben, Bennie, Benny, Benjy, Benjie
Bennet, Bennett => Bennet, Bennett, Ben, Bennie, Benny
Bernard, Barnard => Bernard, Barnard, Bernie, Berney, Barney, Barnie
Bradford => Bradford, Brad, Ford
Bradley => Bradley, Brad
Brandon, Branden
Brian, Bryan, Bryant
Burton => Burton, Burt, Bert
Byron => Byron, Ron, Ronnie, Ronny
Caleb => Caleb, Cal
Calvin => Calvin, Cal, Vin, Vinny
Cameron => Cameron, Cam, Ron, Ronny
Carey, Cary, Carry
Casey, Kasey
Caspar, Casper => Caspar, Casper, Cas, Cass
Cassius => Cassius, Cas, Cass
Cecil => Cecil, Cis
Cedric => Cedric, Ced, Rick, Ricky
Charles => Charles, Charlie, Charley, Chuck, Chas
Chester => Chester, Chet
Christopher, Kristopher, Kristofer => Christopher, Kristopher, Kristofer, Chris, Kris, Cris, Christy, Kit, Kester, Kristof, Toph, Topher
Christian => Christian, Chris, Christy, Kit
Clifford => Clifford, Cliff, Ford
Curtis, Kurtis
Curt, Kurt
Daniel => Daniel, Dan, Danny
Darrell, Darrel, Darryl, Daryl
David => David, Dave, Davie, Davy
Dean, Deane
Dennis, Denis => Dennis, Denis, Den, Denny
Derek, Derrick => Derek, Derrick, Derry, Rick, Ricky
Dominic, Dominick, Dominique => Dominic, Dominick, Dominique, Dom, Nick
Donald => Donald, Don, Donnie, Donny
Duane, Dwayne
Dustin => Dustin, Dusty
Earl, Earle
Edgar => Edgar, Ed, Eddie, Eddy, Ned
Edward => Edward, Ed, Eddie, Eddy, Ned, Ted, Teddy
Edwin => Edwin, Ed, Eddie, Eddy, Ned
Elbert => Elbert, Bert, Bertie
Elijah => Elijah, Eli, Lige
Elliot, Elliott
Elvin, Elwin, Elwyn => Elvin, Elwin, Elwyn, El, Vin, Win
Elwood => Elwood, Woody
Emeric => Emeric, Rick
Emery, Emmery, Emory
Emil, Emile
Emmanuel, Emanuel, Immanuel, Manuel => Emmanuel, Emanuel, Immanuel, Manuel, Manny, Mannie
Eric, Erik, Erick => Eric, Erik, Erick, Rick, Ricky
Ernest, Earnest => Ernest, Earnest, Ernie
Ervin, Erwin, Irvin, Irvine, Irving, Irwin => Ervin, Erwin, Irvin, Irvine, Irving, Irwin, Erv, Vin, Win
Eugene => Eugene, Gene
Everett, Everette
Fabian => Fabian, Fabe, Fab
Ferdinand => Ferdinand, Ferdie, Fred, Freddie
Fernando => Fernando, Ferdie, Fern
Francis => Francis, Frank, Frankie, Franky, Fran
Francisco => Francisco, Frank, Frankie, Franky, Fran
Franklin => Franklin, Franklyn, Frank, Frankie, Franky
Frederick, Frederic, Fredrick, Fredric => Frederick, Frederic, Fredrick, Fredric, Fred, Freddie, Freddy, Rick, Ricky
Gabriel => Gabriel, Gabe, Gabby
Garrett, Garret => Garrett, Garret, Gary, Garry
Geoffrey, Jeffrey => Geoffrey, Jeffrey, Jeff, Geoff
George => George, Georgie, Geordie
Gerald, Gerard => Gerald, Gerard, Gerry, Jerry
Gilbert => Gilbert, Gil, Bert
Glenn, Glen
Gregory => Gregory, Gregor, Greg, Gregg
Harold => Harold, Hal, Harry
Henry => Henry, Harry, Hank
Herbert => Herbert, Herb, Bert, Bertie
Herman => Herman, Manny, Mannie
Howard => Howard, Howie
Hubert => Hubert, Hugh, Bert, Bertie
Hugh => Hugh, Hughie
Isaac => Isaac, Isaak, Ike
Isidore, Isidor, Isadore, Isador => Isidore, Isidor, Isadore, Isador, Izzy
Jackie, Jacky
Jacob, Jake
James, Jim, Jimmy, Jimmie, Jamie, Jem
Jason => Jason, Jay
Jasper => Jasper, Jay
Jeremy => Jeremy, Jerry
Jeremiah => Jeremiah, Jerry
Jerome => Jerome, Jerry
Jesse, Jessie, Jessy
John => Jack, Jackie, Jacky, Johnny
Jonathan => Jonathan, Jon, Jonny
Joseph => Joe, Joey, Jo, Jos, Jody
Joshua => Joshua, Josh
Julian => Julian, Jule, Jules
Julius => Julius, Jule, Jules
Karl, Carl
Kelly, Kelley
Kenneth => Kenneth, Ken, Kenny
Kevin => Kevin, Kev
Kristopher => Kristopher, Kris, Kit, Kester
Lamont => Lamont, Monty, Monte
Lancelot, Launcelot => Lancelot, Launcelot, Lance
Laurence, Lawrence, Lorence => Laurence, Lawrence, Lorence, Lauren, Loren, Larry, Lars, Laurie, Lawrie
Lorenzo => Lorenzo, Enzo, Larry
Leo, Leon => Leo, Leon, Lee
Leonard => Leonard, Leo, Len, Lenny, Lennie
Leopold => Leopold, Leo, Poldie
Leroy => Leroy, Lee, Roy
Leslie, Lesley
Lloyd => Lloyd, Loyd, Loyde, Floyd, Loy, Floy
Floyd => Floyd, Loy, Loyd
Louis, Luis, Lewis => Louis, Luis, Lewis,  Lou, Louie
Luke, Lucas
Malcolm => Malcolm, Mal, Malc, Mac
Mark, Marc, Marcus, Markus
Martin => Martin, Mart, Marty
Marvin, Mervin => Marvin, Mervin, Marv, Merv
Matthew => Matthew, Matt, Mat, Matty, Mattie
Michael, Mike, Mickey
Montague => Montague, Monty, Monte
Montgomery => Montgomery, Monty, Monte
Morris, Maurice => Morris, Maurice, Morry
Mortimer => Mortimer, Mort, Morty
Moses => Moses, Mo, Moe, Mose, Moss
Nathan => Nathan, Nat, Nate
Nathaniel => Nathaniel, Nathan, Nat, Nate
Neal, Neil
Nicholas, Nicolas => Nicholas, Nicolas, Nick, Nicky, Nik
Nik, Nick
Oliver => Oliver, Ollie, Noll, Nollie, Nolly
Oscar => Oscar, Ossy, Ozzy, Ozzie
Oswald => Oswald, Ossy, Ozzie, Ozzy
Patrick => Patrick, Pat, Patty, Paddy, Patsy
Paul => Paul, Pauly
Philip, Phillip => Philip, Phillip Phil, Pip
Ralph => Ralph, Rafe
Randall => Randall, Randy
Randolph => Randolph, Rand, Randy, Dolph
Raphael => Raphael, Rafael, Raff, Raffi, Rafi, Rafe
Raymond, Raymund
Reginald => Reginald, Reg, Reggie, Rex
Reuben => Reuben, Ruben, Rube, Ruby
Reynold => Reynold, Ray
Richard => Richard, Dick, Rick, Ricky, Rich, Richie
Robert => Robert, Bob, Bobbie, Bobby, Dob, Rob, Robbie, Robby, Robin, Bert
Roderic, Roderick => Roderic, Roderick, Rod, Roddy, Rick, Ricky
Roger, Rodger => Roger, Rodger, Rodge
Roland => Roland, Rolly, Roly
Ronald => Ronald, Ron, Ronnie, Ronny
Roscoe => Roscoe, Ross
Reuben, Ruben, Rubin
Rudolph, Rudolf => Rudolph, Rudolf, Rudy, Rolf, Dolph, Dolf
Samson, Sampson => Samson, Sampson, Sam, Sammy
Samuel => Samuel, Sam, Sammy
Scott => Scott, Scotty
Sebastian => Sebastian, Seb, Bass
Sidney, Sydney
Silvester, Sylvester => Silvester, Sylvester, Syl, Vester
Solomon => Solomon, Sol, Solly, Sal
Stephen, Steven => Stephen, Steven, Steve, Stevie
Stuart, Stewart => Stuart, Stewart, Stu, Stew
Terrence, Terence, Terrance => Terrence, Terence, Terrance, Terry
Theodore => Theodore, Ted, Teddy, Theo
Timothy => Timothy, Tim, Timmy
Thomas, Tomas => Thomas, Tomas, Tom, Tommy
Tracy, Tracey
Victor => Victor, Vic, Vick
Vincent => Vincent, Vince, Vin, Vinny
Virgil, Vergil, Virge
Wallace => Wallace, Wally
Walter => Walter, Walt, Wally
Wilbur, Wilber => Wilbur, Wilber, Will, Willie, Willy
Wiley => Wiley, Will, Willie, Willy
Wilfred, Wilfrid => Wilfred, Wilfrid, Will, Willie, Willy, Fred, Freddie, Freddy
Willard => Willard, Will, Willie, Willy
Willis => Willis, Bill, Billy, Billie, Will, Willie, Willy
William => William, Bill, Billy, Billie, Will, Willie, Willy
Wilson => Wilson, Will, Willie, Willy
Winfred => Winfred, Win, Winnie, Winny, Fred, Freddie, Freddy
Winston => Winston, Win, Winnie, Winny
Woodrow => Woodrow, Wood, Woody
Zachariah, Zacharias, Zachary => Zachariah, Zacharias, Zachary, Zack, Zacky, Zach

# Put common nicknames names that should be very expanded here, esp. those that may be either male or female nicknames.

Bob, Bobbie, Bobby, Rob, Robbie, Robby => Roberta, Robbie, Robby, Robin, Robyn, Bobbie, Bobby, Berta, Bertie, Robert, Bob, Bobbie, Bobby, Dob, Rob, Robbie, Robby, Robin, Bert, Roderic, Roderick, Rod, Roddy, Rick, Ricky
