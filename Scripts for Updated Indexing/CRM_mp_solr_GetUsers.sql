USE [YPO_CRM4MWP]
GO
/****** Object:  StoredProcedure [dbo].[mp_solr_GetUsers]    Script Date: 05/07/2015 09:03:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Bennett
-- Create date: 5/7/15
-- Description:	Stored Procedure for User Solr Indexing - Updated for Member Portal
-- =============================================

-- Create Procedure stub if it does not exist
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'mp_solr_GetUsers')
   exec('CREATE PROCEDURE [dbo].[mp_solr_GetUsers] AS BEGIN SET NOCOUNT ON; END')
GO

ALTER PROCEDURE [dbo].[mp_solr_GetUsers]
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

    -- Insert statements for procedure here
	SELECT
	TOP 100  -- TESTING ONLY: Limit the number of results to reduce full index time
	ContactId as 'unique_id'
	,'' as 'YPO_Content'
	,FirstName as 'YPO_First_Name'
	,LastName as 'YPO_Last_Name'
	,MiddleName as 'YPO_Middle_Name'
	,NickName as 'YPO_NickName'
	,AccountIdName as 'CompanyName'
	,JobTitle as 'YPO_Position'
	,YPO_MemberType as 'YPO_WPO_Affiliation'
	,YPO_linkedin as 'LinkedInUrl'
	,a.WebSiteURL as 'Company_Web_Site'
	,[YPO_CRM4MWP].[dbo].udf_StripHTML(cd.YPO_Biography) as 'Bio'
	,cd.YPO_countryoforiginidName as 'YPO_CountryOfOrigin'
	,c.Telephone1 as 'Business'
	,c.MobilePhone as 'Mobile'
	,c.Address1_City + ', ' + c.Address1_StateOrProvince	+ ', ' + c.Address1_Country as 'Location'
	,c.EMailAddress1 as 'Email'
	,YPO_IMNameAOL as 'AOL'
	,YPO_IMNameGTalk as 'GTalk'
	,YPO_IMNameICQ as 'ICQ'
	,YPO_IMNameMSN as 'MSN'
	,YPO_IMNameSkype as 'Skype'
	,YPO_IMNameYahoo as 'Yahoo'
	,c.ypo_ageasoftoday as 'Age'
FROM [YPO_MSCRM].[dbo].[Contact] c WITH (NOLOCK) 
LEFT JOIN [YPO_MSCRM].[dbo].[Account] a WITH (NOLOCK)  on c.ParentCustomerId = a.AccountId
LEFT JOIN [YPO_MSCRM].[dbo].[YPO_contactdemographic] cd WITH (NOLOCK)  on cd.YPO_contactdemographicId = 
		( 
		SELECT TOP 1 YPO_contactdemographicId 
		FROM [YPO_MSCRM].[dbo].[YPO_contactdemographic] WITH (NOLOCK) 
		WHERE ypo_contactid = c.ContactId
		)
WHERE( 
	c.PA_Member = 1
	-- Management Team
	OR (SELECT COUNT(rm.pa_contactid) FROM [YPO_MSCRM].[dbo].[PA_RosterMember] rm WITH (NOLOCK)  WHERE rm.pa_contactid = c.ContactId AND pa_groupid = '06C19EC6-58E3-DE11-8D75-005056923298') > 0
	-- Chapter Administrators
	OR (SELECT COUNT(co.pa_contactid) FROM [YPO_MSCRM].[dbo].[PA_chapterofficer] co WITH (NOLOCK)  WHERE co.pa_contactid=c.ContactId AND pa_titleid ='B0275C20-AEAD-DE11-9A5D-005056921DD3')> 0
	-- Former Member
	OR (SELECT COUNT(rb.pa_customercontactid) FROM [YPO_MSCRM].[dbo].[pa_renewalbilling] rb WITH (NOLOCK) 
		WHERE  rb.pa_customercontactid = c.ContactId
		AND rb.pa_benefitidname = 'YPO-WPO International'
		AND rb.pa_benefitstatus in (3)--terminated
		AND rb.deletionstatecode = 0
		AND rb.statecode = 0
		AND rb.statuscode = 1 ) > 0
	OR (SELECT COUNT(rb.pa_customercontactid) FROM [YPO_MSCRM].[dbo].[pa_renewalbilling] rb WITH (NOLOCK) 
		WHERE  rb.pa_customercontactid = c.ContactId
		AND rb.pa_benefitidname = 'YPO-WPO International'
		AND rb.pa_benefitidname = 'Chapters'
		AND rb.pa_benefitstatus in (3)--terminated 
		AND rb.statecode = 0
		AND rb.statuscode = 1 ) > 0
	-- Grandfathered - Chapter Only
	OR (
		(SELECT COUNT(rb.pa_customercontactid) FROM [YPO_MSCRM].[dbo].[pa_renewalbilling] rb WITH (NOLOCK) 
			WHERE  rb.pa_customercontactid = c.ContactId
			AND rb.pa_chapterbenefit = 1
			AND rb.pa_benefitstatus in (1,2,4)--new,renewed,reinstated
			AND rb.statecode = 0
			AND rb.statuscode = 1 
			AND rb.ypo_current = 1 ) > 0
		AND c.ypo_grandfather = 1)
	-- Spouse/Partners
	OR (SELECT COUNT(cr.CustomerId) FROM [YPO_MSCRM].[dbo].[CustomerRelationship] cr WITH (NOLOCK) 
		WHERE cr.CustomerId = c.ContactId
		AND cr.CustomerRoleId = '0ED54FCA-714F-DE11-8397-005056920556'
		AND	   (SELECT Count(ic.ContactId) 
				FROM [YPO_MSCRM].[dbo].[Contact] ic WITH (NOLOCK) 
				JOIN [YPO_MSCRM].[dbo].[pa_renewalbilling] rb WITH (NOLOCK) ON ic.ContactId = rb.pa_customercontactid 
				WHERE ic.ContactId = cr.PartnerId 
					AND (
						ic.PA_Member = 1 
						OR (
							rb.pa_chapterbenefit = 1
							AND rb.pa_benefitstatus in (1,2,4)--new,renewed,reinstated
							AND rb.statecode = 0
							AND rb.statuscode = 1 
							AND rb.ypo_current = 1)
						)
					) > 0  
		) > 0
	-- Former Member
	OR  (SELECT COUNT(rb.pa_customercontactid) FROM [YPO_MSCRM].[dbo].[pa_renewalbilling] rb WITH (NOLOCK) 
			WHERE  rb.pa_customercontactid = c.ContactId
			AND rb.pa_benefitidname = 'YPO-WPO International'
			AND rb.pa_benefitstatus in (3)--terminated
			AND rb.statecode = 0
			AND rb.statuscode = 1
			AND rb.deletionstatecode = 0 ) > 0
)
	AND c.StatusCode = 1
	AND c.StateCode = 0
	AND c.DeletionStateCode = 0
ORDER BY LastName
END
